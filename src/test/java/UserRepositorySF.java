//package ng.exelon.frankinator2_0.data;
//
//import java.util.List;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.TypedQuery;
//import javax.transaction.Transactional;
//
//import ng.exelon.frankinator2_0.model.User;
//
//import org.hibernate.Criteria;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.criterion.Criterion;
//import org.hibernate.criterion.LogicalExpression;
//import org.hibernate.criterion.Restrictions;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//@Repository
//@Transactional
//public class UserRepositorySF {
//
//	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserRepositorySF.class);
//	
//	@Autowired
//	private SessionFactory sessionFactory;
//	
//	private Session getSession() {
//		return sessionFactory.getCurrentSession();
//	}
//	
//	@SuppressWarnings({"unchecked", "deprecation"})
//	public List<User> findAll() {
//		LOG.warn("Getting all users");
//		Criteria criteria = getSession().createCriteria(User.class);
////		criteria.list()
//		return (List<User>) criteria.list();
//	}
//
//	public User save(User user) {
//		getSession().saveOrUpdate(user);
//		return user;
//	}
//
//	public User findOne(long id) {
//		User user = (User) getSession().get(User.class, id);
//		return user;
//	}
//
//	public void deleteById(Long id) {
//		User user = (User) getSession().get(User.class, id);
//		getSession().delete(user);
//	}
//	
//	/*
//	
//	@PersistenceContext
//	EntityManager em;
//	
//	public User save(User user) {
//		if (user.getId() == null) {
//			em.persist(user);
//		} else {
//			em.merge(user);
//		}
//		return user;
//	}
//	
//	public User findOne(Long id) {
//		return em.find(User.class, id);
//	}
//
//	public void deleteById(Long id) {
//		User student = findOne(id);
//		em.remove(student);
//	}
//	
//	public List<User> findAll() {
//		LOG.warn("Getting all users");
//		TypedQuery<User> customQuery = em.createQuery("select u from User u", User.class);
//		return customQuery.getResultList();
////		TypedQuery<User> namedQuery = em.createNamedQuery("find_all_persons", User.class);
////		return namedQuery.getResultList();
//	}
//	*/
//}
