package com.najcom.client.db;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DBConfig {
    @Bean
	public DataSource dataSource() {
	        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
	        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
	        dataSourceBuilder.url("jdbc:sqlite:client.db");
//	        dataSourceBuilder.url("jdbc:sqlite:C:\\wamp\\www\\sts-bundle\\workbench\\client\\test.db");
	        return dataSourceBuilder.build();   
	}
}
