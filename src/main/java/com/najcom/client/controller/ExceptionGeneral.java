package com.najcom.client.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionGeneral {

	@ExceptionHandler
    void handleConstraintViolationException(ConstraintViolationException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.OK.value());
    }
	
}