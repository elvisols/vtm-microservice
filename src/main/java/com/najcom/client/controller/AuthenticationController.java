package com.najcom.client.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.service.LoginService;

@RestController
public class AuthenticationController {

    @Autowired
    LoginService loginService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getDetails(RequestEntity<Map<String, String>> req) {
    	Map<String, String> requestBody = req.getBody();
        Map<String, Object> details = loginService.doAuth(requestBody.getOrDefault("username", "Undefine"), requestBody.getOrDefault("password", "abc123"));
		return new ResponseEntity<Map<String, Object>>(details, HttpStatus.OK);
    }
    
}

