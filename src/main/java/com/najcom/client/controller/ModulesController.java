package com.najcom.client.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.service.ModuleService;

@RestController
public class ModulesController {

    @Autowired
    ModuleService moduleService;
    
//    @Value("#{'${vtm.states}'.split(',')}") 
    private List<String> states;

    /** Partly implemented **/
    @RequestMapping(path = "/modules", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getVtms() {
    	Map<String, Object> modules = moduleService.findModules();
    	return new ResponseEntity<Map<String, Object>>(modules, HttpStatus.OK);
    }
    
//    @RequestMapping(path = "/states", method = RequestMethod.GET)
//    public ResponseEntity<List<String>> getStates() {
//    	System.out.println("State: " + states.get(0));
//    	return new ResponseEntity<List<String>>(states, HttpStatus.OK);
//    }
    
}
