package com.najcom.client.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.service.BalanceService;
import com.najcom.client.util.MyError;


@RestController
@RequestMapping("balance")
public class BalanceController {

    @Autowired
    BalanceService balanceService;
    
	@RequestMapping(path = "{id}", method = RequestMethod.GET, headers="Accept=application/json")
	public ResponseEntity<Map<String, Object>> getUserBalance(@PathVariable("id") String id, HttpServletRequest request) {
		Map<String, Object> balance = balanceService.getBalance(id, request);
		return new ResponseEntity<Map<String, Object>>(balance, HttpStatus.OK);
	}
    
}

