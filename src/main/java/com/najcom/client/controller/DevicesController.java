package com.najcom.client.controller;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.asprise.imaging.core.Imaging;
import com.asprise.imaging.core.Request;
import com.asprise.imaging.core.RequestOutputItem;
import com.asprise.imaging.core.Result;
import com.asprise.imaging.core.scan.twain.TwainConstants;
import com.asprise.imaging.scan.ui.workbench.AspriseScanUI;
import com.najcom.client.model.Persistence;
import com.najcom.client.service.PersistenceDao;
import com.najcom.client.util.StreamGobbler;

@RestController
@RequestMapping("devices")
public class DevicesController {

	private final int ALLOWED_ACCOUNT_TIME = 10 * 60; // In seconds
	
	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Value("${vtm.file.root.path}")
	private String rootPath;
	
	@Value("${vtm.file.capture.path}")
	private String capturePath;
	
	@Value("${vtm.lib.path}")
	private String libPath;
	
	@Autowired
    PersistenceDao persistenceDao;
	
	@RequestMapping(path = "/scanner/meansOfId", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getScanner(RequestEntity<Map<String, String>> req, HttpServletRequest request) throws java.io.IOException {
		
		Map<String, Object> msg = new HashMap<>();
    	List<Persistence> lstP = persistenceDao.findByRef(req.getBody().getOrDefault("reference", "0").toString());
    	Persistence p = lstP.isEmpty() ? null : lstP.get(0);
		if(p == null || isExpired(p.getDate())) {
			msg.put("status", 400);
	    	msg.put("hash", req.getBody().getOrDefault("reference", "0"));
	    	msg.put("message", "invalid or expired reference");
	    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		try {
	    	Result result = new AspriseScanUI().setRequest(new Request()
			  .setTwainCap( // Scan in color:
			    TwainConstants.ICAP_PIXELTYPE, TwainConstants.TWPT_RGB)
			  .setTwainCap( // Paper size: US letter
			    TwainConstants.ICAP_SUPPORTEDSIZES, TwainConstants.TWSS_USLETTER)
			  .addOutputItem(
			     new RequestOutputItem(Imaging.OUTPUT_SAVE_THUMB, Imaging.FORMAT_PNG)
			     	.setSavePath(capturePath + File.separator + p.getRef() + "/identification.png")
			  )
			 ).showDialog(null, "Scan", true, null); // owner can be null
	
			List<File> files = result.getImageFiles(); // Gets files
			List<BufferedImage> imgs = result.getImages(); // to display on UI
		} catch (Exception ex) {
			// Add below to bypass expired asprise
			int width = 250; int height = 250;
	        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	        Graphics2D g2d = bufferedImage.createGraphics();
	        // fill all the image with white
	        g2d.setColor(Color.white);
	        g2d.fillRect(0, 0, width, height);
	        // create a circle with black
	        g2d.setColor(Color.black);
	        g2d.fillOval(0, 0, width, height);
	        g2d.setColor(Color.yellow);
	        g2d.drawString("Identity Picture", 50, 120);
	        g2d.dispose();
			FileOutputStream fos = FileUtils.openOutputStream(new File(capturePath + File.separator + p.getRef() + "/identification.png"), true);
//	        File file = new File(capturePath + File.separator + p.getRef() + "/identification.png");
	        ImageIO.write(bufferedImage, "png", fos);

			msg.put("status", 200);
	    	msg.put("hash", p.getRef());
	    	msg.put("message", "Scanner exception returned.");
			return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		msg.put("status", 200);
    	msg.put("hash", p.getRef());
    	msg.put("message", "Scanner returned.");
		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
    }
	
	@RequestMapping(path = "/scanner/passport", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getScannerPassport(RequestEntity<Map<String, String>> req, HttpServletRequest request) throws java.io.IOException {
		
		Map<String, Object> msg = new HashMap<>();
		List<Persistence> lstP = persistenceDao.findByRef(req.getBody().getOrDefault("reference", "0").toString());
    	Persistence p = lstP.isEmpty() ? null : lstP.get(0);
		if(p == null || isExpired(p.getDate())) {
			msg.put("status", 400);
	    	msg.put("hash", req.getBody().getOrDefault("reference", "0"));
	    	msg.put("message", "invalid or expired reference");
	    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		
		try {
			Result result = new AspriseScanUI().setRequest(new Request()
			.setTwainCap( // Scan in color:
					TwainConstants.ICAP_PIXELTYPE, TwainConstants.TWPT_RGB)
					.setTwainCap( // Paper size: US letter
							TwainConstants.ICAP_SUPPORTEDSIZES, TwainConstants.TWSS_USLETTER)
								.addOutputItem(
										new RequestOutputItem(Imaging.OUTPUT_SAVE_THUMB, Imaging.FORMAT_PNG)
										.setSavePath(capturePath + File.separator + p.getRef() + "/passport.png")
											)
					).showDialog(null, "Scan", true, null); // owner can be null
			
			List<File> files = result.getImageFiles(); // Gets files
			List<BufferedImage> imgs = result.getImages(); // to display on UI
		} catch (Exception ex) {
			// Add below to bypass expired asprise
			int width = 250; int height = 250;
	        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	        Graphics2D g2d = bufferedImage.createGraphics();
	        // fill all the image with white
	        g2d.setColor(Color.white);
	        g2d.fillRect(0, 0, width, height);
	        // create a circle with black
	        g2d.setColor(Color.black);
	        g2d.fillOval(0, 0, width, height);
	        g2d.setColor(Color.yellow);
	        g2d.drawString("Passport Picture", 50, 120);
	        g2d.dispose();
			FileOutputStream fos = FileUtils.openOutputStream(new File(capturePath + File.separator + p.getRef() + "/passport.png"), true);
//	        File file = new File(capturePath + File.separator + p.getRef() + "/passport.png");
	        ImageIO.write(bufferedImage, "png", fos);
			msg.put("status", 200);
	    	msg.put("hash", p.getRef());
	    	msg.put("message", "Scanner exception returned.");
			return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		msg.put("status", 200);
    	msg.put("hash", p.getRef());
    	msg.put("message", "Scanner returned.");
		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/scanner/utility", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getScannerUtility(RequestEntity<Map<String, String>> req, HttpServletRequest request) throws java.io.IOException {
		Map<String, Object> msg = new HashMap<>();
		List<Persistence> lstP = persistenceDao.findByRef(req.getBody().getOrDefault("reference", "0").toString());
    	Persistence p = lstP.isEmpty() ? null : lstP.get(0);
		if(p == null || isExpired(p.getDate())) {
			msg.put("status", 400);
	    	msg.put("hash", req.getBody().getOrDefault("reference", "0"));
	    	msg.put("message", "invalid or expired reference");
	    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		
		try {
			Result result = new AspriseScanUI().setRequest(new Request()
			.setTwainCap( // Scan in color:
					TwainConstants.ICAP_PIXELTYPE, TwainConstants.TWPT_RGB)
					.setTwainCap( // Paper size: US letter
							TwainConstants.ICAP_SUPPORTEDSIZES, TwainConstants.TWSS_USLETTER)
							.addOutputItem(
								new RequestOutputItem(Imaging.OUTPUT_SAVE, Imaging.FORMAT_PNG)
								.setSavePath(capturePath + File.separator + p.getRef() + "/utility.png")
								)
					).showDialog(null, "Scan", true, null); // owner can be null
			
			List<File> files = result.getImageFiles(); // Gets files
			List<BufferedImage> imgs = result.getImages(); // to display on UI
		} catch (Exception ex) {
			// Add below to bypass expired asprise
			int width = 250; int height = 250;
	        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	        Graphics2D g2d = bufferedImage.createGraphics();
	        // fill all the image with white
	        g2d.setColor(Color.white);
	        g2d.fillRect(0, 0, width, height);
	        // create a circle with black
	        g2d.setColor(Color.black);
	        g2d.fillOval(0, 0, width, height);
	        g2d.setColor(Color.yellow);
	        g2d.drawString("Utility Image", 50, 120);
	        g2d.dispose();
			FileOutputStream fos = FileUtils.openOutputStream(new File(capturePath + File.separator + p.getRef() + "/utility.png"), true);
//	        File file = new File(capturePath + File.separator + p.getRef() + "/utility.png");
	        ImageIO.write(bufferedImage, "png", fos);
			msg.put("status", 200);
	    	msg.put("hash", p.getRef());
	    	msg.put("message", "Scanner exception returned.");
			return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
		msg.put("status", 200);
    	msg.put("hash", p.getRef());
    	msg.put("message", "Scanner returned.");
		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
	}
    
    @RequestMapping(path = "/camera", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getCamera(RequestEntity<Map<String, String>> req, HttpServletRequest request) throws java.io.IOException, InterruptedException{

    	Map<String, Object> msg = new HashMap<>();
    	List<Persistence> lstP = persistenceDao.findByRef(req.getBody().getOrDefault("reference", "0").toString());
    	Persistence p = lstP.isEmpty() ? null : lstP.get(0);
		if(p == null || isExpired(p.getDate())) {
			msg.put("status", 400);
	    	msg.put("hash", req.getBody().getOrDefault("reference", "0"));
	    	msg.put("message", "invalid or expired reference");
	    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
		}
    	
    	 try
         {  
    		 FileOutputStream fos = FileUtils.openOutputStream(new File(rootPath + "/logs/devices.log"), true);
             Runtime rt = Runtime.getRuntime();
             Process pr = rt.exec("java -cp java -cp target/classes;" + libPath + "\\* com.najcom.client.controller.StartCamera " + capturePath + File.separator + p.getRef());
             // any error message?
             StreamGobbler errorGobbler = new StreamGobbler(pr.getErrorStream(), "ERROR");            
             
             // any output?
             StreamGobbler outputGobbler = new StreamGobbler(pr.getInputStream(), "OUTPUT", fos);
                 
             // kick them off
             errorGobbler.start();
             outputGobbler.start();
                                     
             // any error???
             int exitVal = pr.waitFor();
             fos.flush();
             fos.close();
             if(exitVal == 0) {
            	 msg.put("status", 200);
            	 msg.put("hash", p.getRef());
            	 msg.put("message", "Camera thread completed successfully.");
             } else {
            	 msg.put("status", 500);
            	 msg.put("message", "Oops! something went wrong with camera thread.");
             }
         } catch (Throwable t)
           {
             t.printStackTrace();
           }  
    	
    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/slipPrinter/balance", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getPrinter(RequestEntity<Map<String, String>> req) {
    	
    	Map<String, Object> msg = new HashMap<>();
	   	
    	try {    
	         FileOutputStream fos = FileUtils.openOutputStream(new File(rootPath + "/logs/devices.log"), true);
	         Runtime rt = Runtime.getRuntime();
	         Process p = rt.exec("java -cp java -cp target/classes com.najcom.client.controller.StartPrinter " + rootPath);
	         // any error message?
	         StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");            
	         
	         // any output?
	         StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT", fos);
	             
	         // kick them off
	         errorGobbler.start();
	         outputGobbler.start();
	                                 
	         // any error???
	         int exitVal = p.waitFor();
	         fos.flush();
	         fos.close();
	         if(exitVal == 0) {
            	 msg.put("status", 200);
            	 msg.put("message", "Printer thread completed successfully.");
             } else {
            	 msg.put("status", 500);
            	 msg.put("message", "Oops! something went wrong with printer thread.");
             }     
     	} catch (Throwable t) {
    	 	t.printStackTrace();
     	}
    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/a4Printer/statement", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getA4Printer(RequestEntity<Map<String, String>> req) {

    	Map<String, Object> msg = new HashMap<>();
    	
    	try {    
    		FileOutputStream fos = FileUtils.openOutputStream(new File(rootPath + "/logs/devices.log"), true);
    		Runtime rt = Runtime.getRuntime();
    		Process p = rt.exec("java -cp java -cp target/classes com.najcom.client.controller.StartPrinter " + rootPath);
    		// any error message?
    		StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");            
    		
    		// any output?
    		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT", fos);
    		
    		// kick them off
    		errorGobbler.start();
    		outputGobbler.start();
    		
    		// any error???
    		int exitVal = p.waitFor();
    		fos.flush();
    		fos.close();
    		if(exitVal == 0) {
    			msg.put("status", 200);
    			msg.put("message", "Printer thread completed successfully.");
    		} else {
    			msg.put("status", 500);
    			msg.put("message", "Oops! something went wrong with printer thread.");
    		}     
    	} catch (Throwable t) {
    		t.printStackTrace();
    	}
    	return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
    }
    
    private boolean isExpired(String startedAt) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = dateFormat.format(new Date());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(startedAt);
			d2 = format.parse(now);

			//in milliseconds
			long diff = d2.getTime() - d1.getTime();
			return ((diff/1000) > ALLOWED_ACCOUNT_TIME) ? true : false;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
    }
	
}
