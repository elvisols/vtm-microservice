package com.najcom.client.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.service.StaticService;


@RestController
@RequestMapping("account")
public class StaticController {

    @Autowired
    StaticService staticService;
    
	@RequestMapping(path = "states", method = RequestMethod.GET, headers="Accept=application/json")
	public ResponseEntity<Map<String, Object>> getStates(HttpServletRequest request) {
		Map<String, Object> states = staticService.getStates(request);
		return new ResponseEntity<Map<String, Object>>(states, HttpStatus.OK);
	}
	
	@RequestMapping(path = "titles", method = RequestMethod.GET, headers="Accept=application/json")
	public ResponseEntity<Map<String, Object>> getTitles(HttpServletRequest request) {
		Map<String, Object> titles = staticService.getTitles(request);
		return new ResponseEntity<Map<String, Object>>(titles, HttpStatus.OK);
	}
    
}

