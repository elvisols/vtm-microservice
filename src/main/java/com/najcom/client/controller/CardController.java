package com.najcom.client.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.model.Card;
import com.najcom.client.service.CardService;
import com.najcom.client.util.MyError;


@RestController
@RequestMapping("card")
public class CardController {

    @Autowired
    CardService cardService;
    
    @Autowired
    Validator validator;

    @RequestMapping(path = "/issuance", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> sendIssuance(RequestEntity<Card> req, HttpServletRequest request) {
    	Card cardBody = req.getBody();
        MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Card.class.getName());
    	validator.validate(cardBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.EXPECTATION_FAILED);
    	} 
        Map<String, Object> card = cardService.sendIssuance(cardBody, request);
		return new ResponseEntity<Map<String, Object>>(card, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/hotlisting", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> sendHotlisting(RequestEntity<Card> req, HttpServletRequest request) {
    	Card cardBody = req.getBody();
        MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Card.class.getName());
    	validator.validate(cardBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.EXPECTATION_FAILED);
    	} 
        Map<String, Object> card = cardService.sendHotlisting(cardBody, request);
		return new ResponseEntity<Map<String, Object>>(card, HttpStatus.OK);
    }
    
}

