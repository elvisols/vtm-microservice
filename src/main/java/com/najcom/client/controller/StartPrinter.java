package com.najcom.client.controller;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.najcom.client.util.TextAreaPrintingDemo;

public class StartPrinter {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                try {
                	JFrame f = new TextAreaPrintingDemo(args[0]);
                    f.setLocationRelativeTo(null);
                    f.setVisible(true);
                } catch (Exception e) {
                	e.printStackTrace();
                }
            }
        });
	}

}
