package com.najcom.client.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.model.AddressDetail;
import com.najcom.client.model.DocumentDetail;
import com.najcom.client.model.Persistence;
import com.najcom.client.model.PersonalDetail;
import com.najcom.client.model.Account;
import com.najcom.client.service.AccountService;
import com.najcom.client.service.PersistenceDao;
import com.najcom.client.util.AccountOpener;
import com.najcom.client.util.MyError;
import com.najcom.client.util.Requester;


@RestController
@RequestMapping("account")
public class AccountController {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
    @Autowired
    AccountService accountService;
    
    @Autowired
    Validator validator;
    
    @Autowired
    PersistenceDao persistenceDao;

    @RequestMapping(path = "/startAccountOpenWorkFlow", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> startAccountOpenWorkFlow(RequestEntity<Map<String, String>> req, HttpServletRequest request) throws ClientProtocolException, IOException {
    	
    	String reference = UUID.randomUUID().toString().replace("-", "");
    	// Log workflow
    	Persistence newP = new Persistence();
    	newP.setAction("Started Account Opening..." + request.getRemoteAddr() + " @RemoteHost:" + request.getRemoteHost());
    	newP.setRef(reference);
    	newP.setUsr(String.valueOf(newP.hashCode()));
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = dateFormat.format(new Date());
    	newP.setDate(now);
    	persistenceDao.save(newP);
        
    	Map<String, Object> msg = new HashMap<>();
        msg.put("status", 200);
        msg.put("message", "Success");
        msg.put("hash", reference);
		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/open", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> openAccount(RequestEntity<Account> req, final HttpServletRequest request) throws ClientProtocolException, IOException {
    	// create a session
        request.getSession(true);
    	System.out.println("My reference is " + request.getAttribute("reference"));
    	Account accountBody = req.getBody();
    	MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Account.class.getName());
    	validator.validate(accountBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.OK);
    	} 
    	
    	AccountOpener aOpener =  this.getAccountOpener(accountBody);
    	aOpener.setToken(request.getHeader("token"));
    	// Serialize account object and save in SQLite
    	Persistence newP = new Persistence();
    	newP.setAction("object");
    	newP.setProcessed(0);
    	newP.setRef(aOpener.getReference());

    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(aOpener);
        oos.flush(); oos.close(); bos.close();
    	
        newP.setAccobj(bos.toByteArray());
    	newP.setUsr(String.valueOf(newP.hashCode()));
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = dateFormat.format(new Date());
    	newP.setDate(now);
    	Persistence nP = persistenceDao.save(newP);
    	
    	Map<String, Object> account = accountService.openAccount(aOpener);
    	if(account.get("status").equals("200") ) {
    		// Update logs
    		System.out.println("Updating processed to 1");
    		nP.setProcessed(1);
    		persistenceDao.save(nP);
    	} else {
    		account.put("message", "Successful! Your request has been queued you will get an email shortly");
    	}
    	return new ResponseEntity<Map<String, Object>>(account, HttpStatus.OK);

    }
    
    // See http://www.baeldung.com/spring-scheduled-tasks
    @Scheduled(fixedDelayString = "${fixedDelay.in.milliseconds}")
    public void print() {
    	// Retrieve object
    	Persistence p = persistenceDao.findByActionAndProcessed("object", 0);
		if(p != null) {
			ByteArrayInputStream bais;
			ObjectInputStream ins;
			try {
				bais = new ByteArrayInputStream(p.getAccobj());
				ins = new ObjectInputStream(bais);
				AccountOpener ao =(AccountOpener) ins.readObject();
				System.out.println("Retrying account Opener Object:");
				System.out.println(ao);
				Map<String, Object> account = accountService.openAccount(ao);
				// Check...
				if(account.get("status").equals("200") ) {
		    		// Update logs
		    		System.out.println("Updating processed to 1");
		    		p.setProcessed(1);
		    		persistenceDao.save(p);
		    	} 
				ins.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No serialized object record found!");
		}      
    }
    
    @RequestMapping(path = "/inter/{id}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> interAccount(@PathVariable("id") String id, HttpServletRequest request) {
    	Map<String, Object> account = accountService.interAccountDetails(id, request);
    	return new ResponseEntity<Map<String, Object>>(account, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/intra/{id}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> intraAccount(@PathVariable("id") String id, HttpServletRequest request) {
    	Map<String, Object> account = accountService.intraAccountDetails(id, request);
    	return new ResponseEntity<Map<String, Object>>(account, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/statements", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> accountStatement(HttpServletRequest request) {
    	Map<String, Object> account = accountService.getAccountStatement(request);
    	return new ResponseEntity<Map<String, Object>>(account, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/meansOfId", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getMeansOfId(HttpServletRequest request) {
    	Map<String, Object> moi = accountService.getMeansOfId(request);
    	return new ResponseEntity<Map<String, Object>>(moi, HttpStatus.OK);
    }
    
    private AccountOpener getAccountOpener(Account accountBody) {
    	AccountOpener aOpener =  new AccountOpener();
    	aOpener.setReference(accountBody.getReference());
    	aOpener.setTitle(accountBody.getPersonalDetail().getTitle());
    	aOpener.setFirstname(accountBody.getPersonalDetail().getFirstName());
    	aOpener.setMiddlename(accountBody.getPersonalDetail().getMiddleName());
    	aOpener.setLastname(accountBody.getPersonalDetail().getLastName());
    	aOpener.setAddress1(accountBody.getAddressDetail().getAddress1());
    	aOpener.setAddress2(accountBody.getAddressDetail().getAddress2());
    	aOpener.setAddresstype(accountBody.getAddressDetail().getAddressType());
    	aOpener.setCity(accountBody.getAddressDetail().getCity());
    	aOpener.setState(accountBody.getAddressDetail().getState());
    	aOpener.setEmail(accountBody.getPersonalDetail().getEmail());
    	aOpener.setPhone(accountBody.getPersonalDetail().getMobileNumber());
    	aOpener.setDateofbirth(accountBody.getPersonalDetail().getDateOfBirth());
    	aOpener.setMaritalstatus(accountBody.getPersonalDetail().getMaritalStatus());
    	aOpener.setSex(accountBody.getPersonalDetail().getSex());
    	aOpener.setStateoforigin(accountBody.getPersonalDetail().getStateOfOrigin());
    	aOpener.setBvn(accountBody.getPersonalDetail().getBvn());
    	aOpener.setTaxidnumber(accountBody.getPersonalDetail().getTin());
    	aOpener.setSpousename(accountBody.getPersonalDetail().getSpouseName());
    	aOpener.setReligion(accountBody.getPersonalDetail().getReligion());
    	aOpener.setBirthcountry(accountBody.getPersonalDetail().getBirthCountry());
    	aOpener.setResidentcountry(accountBody.getPersonalDetail().getResidentCountry());
    	aOpener.setUtilitybill(accountBody.getDocumentDetail().getUtilityHash());
    	aOpener.setIdentification(accountBody.getDocumentDetail().getIdHash());
    	aOpener.setPassport(accountBody.getDocumentDetail().getPassportHash());
    	
    	System.out.println("Phone: " + aOpener.getPhone());
    	System.out.println("Mobile number: " + accountBody.getPersonalDetail().getMobileNumber());
    	
    	return aOpener;
    	
    }
    
    @ExceptionHandler
    void handleConstraintViolationException(Exception e, HttpServletResponse response) throws IOException {
    	response.sendError(HttpStatus.BAD_REQUEST.value());
    	response.setStatus(200);
    }
    
}

