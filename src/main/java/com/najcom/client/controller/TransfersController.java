package com.najcom.client.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.client.model.Transfer;
import com.najcom.client.service.TransferService;
import com.najcom.client.util.MyError;


@RestController
@RequestMapping("transfer")
public class TransfersController {

    @Autowired
    TransferService transferService;
    
    @Autowired
    Validator validator;

    @RequestMapping(path = "/interbank", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> doInterTransfer(RequestEntity<Transfer> req, HttpServletRequest request) {
    	Transfer transferBody = req.getBody();
        MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Transfer.class.getName());
    	validator.validate(transferBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.EXPECTATION_FAILED);
    	} 
        Map<String, Object> transfer = transferService.transferInter(transferBody, request);
		return new ResponseEntity<Map<String, Object>>(transfer, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/intrabank", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> doIntraTransfer(RequestEntity<Transfer> req, HttpServletRequest request) {
    	Transfer transferBody = req.getBody();
        MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Transfer.class.getName());
    	validator.validate(transferBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.EXPECTATION_FAILED);
    	} 
        Map<String, Object> transfer = transferService.transferIntra(transferBody, request);
		return new ResponseEntity<Map<String, Object>>(transfer, HttpStatus.OK);
    }
    
    @RequestMapping(path = "/ownaccount", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> doOwnAccountTransfer(RequestEntity<Transfer> req, HttpServletRequest request) {
    	Transfer transferBody = req.getBody();
    	MapBindingResult errors = new MapBindingResult(new HashMap<String, String>(), Transfer.class.getName());
    	validator.validate(transferBody, errors);
    	if(errors.hasErrors()) {
    		return new ResponseEntity<Map<String, Object>>(MyError.parse(errors), HttpStatus.EXPECTATION_FAILED);
    	} 
    	Map<String, Object> transfer = transferService.transferOwnAccount(transferBody, request);
    	return new ResponseEntity<Map<String, Object>>(transfer, HttpStatus.OK);
    }
    
    @RequestMapping(path = "credit-account/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	public ResponseEntity<Map<String, Object>> getCreditAccIntra(@PathVariable("id") String id, HttpServletRequest request) {
		Map<String, Object> caIntra = transferService.getCreditAccount(id, request);
		return new ResponseEntity<Map<String, Object>>(caIntra, HttpStatus.OK);
	}
    
    @RequestMapping(path = "credit-account/{id}/bankcode/{code}", method = RequestMethod.GET, headers="Accept=application/json")
    public ResponseEntity<Map<String, Object>> getCreditAccInter(@PathVariable("id") String id, @PathVariable("code") String code, HttpServletRequest request) {
    	Map<String, Object> caInter = transferService.getCreditAccount(id, code, request);
    	return new ResponseEntity<Map<String, Object>>(caInter, HttpStatus.OK);
    }
    
    @RequestMapping(path = "banks", method = RequestMethod.GET, headers="Accept=application/json")
    public ResponseEntity<Map<String, Object>> getUserBalance(HttpServletRequest request) {
    	Map<String, Object> banks = transferService.getBanks(request);
    	return new ResponseEntity<Map<String, Object>>(banks, HttpStatus.OK);
    }
    
}

