package com.najcom.client;

import java.text.SimpleDateFormat;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.najcom.client.model.Persistence;
import com.najcom.client.service.PersistenceDao;
import static java.lang.System.exit;

@SpringBootApplication
public class BootMachine {

	public static void main(String[] args) {
		
//		SpringApplication.run(BootMachine.class, args);
		SpringApplicationBuilder builder = new SpringApplicationBuilder(BootMachine.class);
		builder.headless(false);
		ConfigurableApplicationContext context = builder.run(args);

	}
	/*
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    DataSource dataSource;

    @Autowired
    PersistenceDao persistenceDao;

	
	@Transactional(readOnly = true)
    public void run(String... args) throws Exception {

        System.out.println("DATASOURCE = " + dataSource);

        System.out.println("\n1.findAll()...");
        for (Persistence customer : persistenceDao.findAll()) {
            System.out.println(customer);
        }

        System.out.println("\n2.findByEmail(String email)...");
        for (Persistence customer : persistenceDao.findByRef("2")) {
            System.out.println(customer);
        }

        System.out.println("\n3.findByDate(Date date)...");
        for (Persistence customer : persistenceDao.findByDate(sdf.parse("2017-02-12"))) {
            System.out.println(customer);
        }

        // For Stream, need @Transactional
//        System.out.println("\n4.findByEmailReturnStream(@Param(\"email\") String email)...");
//        try (Stream<Persistence> stream = persistenceDao.findByActionReturnStream("333@yahoo.com")) {
//            stream.forEach(x -> System.out.println(x));
//        }

        System.out.println("Done!");

        exit(0);
    }
*/
}
