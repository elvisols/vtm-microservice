package com.najcom.client.util;

public class User {

	private String name;
    private String developer;

    public User(String name, String developer) {
            this.name = name;
            this.developer = developer;
    }

    public String getName() {
            return name;
    }

    public String getDeveloper() {
            return developer;
    }
    
}
