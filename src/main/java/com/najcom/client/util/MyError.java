package com.najcom.client.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;

public class MyError {

	public static Map<String, Object> parse(MapBindingResult errors) {
		List<FieldError> errs = errors.getFieldErrors();
		Map<String, Object> msg = new HashMap<>();
		Map<String, Object> errorMsg = new HashMap<>();
	    for (FieldError error : errs) {
	    	errorMsg.put(error.getField(), "["+error.getRejectedValue()+"] " + error.getDefaultMessage());
	    }
	    msg.put("status", 417);
	    msg.put("message", "Expectations failed.");
	    msg.put("error", errorMsg);
	    return msg;
	}
	
}
