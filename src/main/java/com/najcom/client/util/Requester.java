package com.najcom.client.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Requester {
	
	@Value("${vtm.server.path}")
	private String serverUrl;

	public static String getUsername(String path, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(path + "/auth/getusername");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData.getOrDefault("username", "null").toString();
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return "null";
	}

}
