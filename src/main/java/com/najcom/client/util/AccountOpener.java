package com.najcom.client.util;

import java.util.Date;

public class AccountOpener implements java.io.Serializable {

	private String reference;
	
	private String address1;
	
	private String address2;
	
	private String addresstype;
	
	private String city;
	
	private String state;
	
	private String utilitybill;
	
	private String identification;
	
	private String passport;
	
	private String title;

	private String email;

	private String firstname;

	private String lastname;
	
	private String middlename;
	
	private String maritalstatus;
	
	private String sex;
	
	private String stateoforigin;
	
	private String bvn;
	
	private String taxidnumber;
	
	private String spousename;
	
	private String residentcountry;
	
	private String birthcountry;
	
	private Date dateofbirth;

	private String phone;

	private String religion;

	private String token;
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddresstype() {
		return addresstype;
	}

	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBvn() {
		return bvn;
	}

	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getUtilitybill() {
		return utilitybill;
	}

	public void setUtilitybill(String utilitybill) {
		this.utilitybill = utilitybill;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getMaritalstatus() {
		return maritalstatus;
	}

	public void setMaritalstatus(String maritalstatus) {
		this.maritalstatus = maritalstatus;
	}

	public String getStateoforigin() {
		return stateoforigin;
	}

	public void setStateoforigin(String stateoforigin) {
		this.stateoforigin = stateoforigin;
	}

	public String getSpousename() {
		return spousename;
	}

	public void setSpousename(String spousename) {
		this.spousename = spousename;
	}

	public String getResidentcountry() {
		return residentcountry;
	}

	public void setResidentcountry(String residentcountry) {
		this.residentcountry = residentcountry;
	}

	public String getBirthcountry() {
		return birthcountry;
	}

	public void setBirthcountry(String birthcountry) {
		this.birthcountry = birthcountry;
	}

	public Date getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTaxidnumber() {
		return taxidnumber;
	}

	public void setTaxidnumber(String taxidnumber) {
		this.taxidnumber = taxidnumber;
	}
	
	public String toString() {
		return 	"Reference: " + reference + "\n"
		 + "Address One: " + address1 + "\n"
		 + "Address Two: " + address2 + "\n"
		 + "Addresstype Type: " + addresstype + "\n"
		 + "City: " + city + "\n"
		 + "State: " + state + "\n"
		 + "Utility bill: " + utilitybill + "\n"
		 + "Identification: " + identification + "\n"
		 + "Passport: " + passport + "\n"
		 + "Title: " + title + "\n"
		 + "Email: " + email + "\n"
		 + "Firstname: " + firstname + "\n"
		 + "Lastname: " + lastname + "\n"
		 + "Middlename: " + middlename + "\n"
		 + "Marital Status: " + maritalstatus + "\n"
		 + "Sex: " + sex + "\n"
		 + "State Of Origin: " + stateoforigin + "\n"
		 + "BVN: " + bvn + "\n"
		 + "Taxid Number: " + taxidnumber + "\n"
		 + "Spouse Name: " + spousename + "\n"
		 + "Resident Country: " + residentcountry + "\n"
		 + "Birthday country: " + birthcountry + "\n"
		 + "Date Of Birth: " + dateofbirth + "\n"
		 + "Phone: " + phone + "\n"
		 + "Token: " + token + "\n"
		 + "Religion: " + religion;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
