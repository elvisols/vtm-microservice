package com.najcom.client.util;

import java.security.Permission;

public class MySecurityManager extends SecurityManager {
  @Override public void checkExit(int status) {
    throw new SecurityException();
  }
  @Override
  public void checkPermission(Permission permission) {
		if ("exitVM".equals(permission.getName())) {
			throw new SecurityException("System.exit attempted and blocked.");
		} else {
			System.out.println("Non system exit");
		}
	}
}
