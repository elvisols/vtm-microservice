package com.najcom.client.util;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamDiscoveryEvent;
import com.github.sarxos.webcam.WebcamDiscoveryListener;
import com.github.sarxos.webcam.WebcamEvent;
import com.github.sarxos.webcam.WebcamListener;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamPicker;
import com.github.sarxos.webcam.WebcamResolution;

public class Webcam3 extends JFrame implements Runnable, WebcamListener, WindowListener, UncaughtExceptionHandler, ItemListener, WebcamDiscoveryListener {
	private static final long serialVersionUID = 1L;

	
	private Webcam webcam = null;
	private WebcamPanel panel = null;
	private WebcamPicker picker = null;
	private String capturePath;
	
	public Webcam3(String capturePath) {
		this.capturePath = capturePath;
	}
		
	@Override
	public void run() {

		Webcam.addDiscoveryListener(this);
		
		setTitle("Java Webcam Capture POC");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setAlwaysOnTop(true);
		setAutoRequestFocus(true);
		setSize(getMaximumSize());
//		setLocationRelativeTo(null);
		setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		// Add the menu bar.
	    JMenuBar mb = new JMenuBar();
	    JMenu file = new JMenu("File", true);
	    file.add(new CaptureImageAction(this.capturePath)).setAccelerator(
	        KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK));
	    file.addSeparator();
	    file.add(new FileQuitAction()).setAccelerator(
	        KeyStroke.getKeyStroke(KeyEvent.VK_Q, Event.CTRL_MASK));
	    mb.add(file);
	    setJMenuBar(mb);
	    
		addWindowListener(this);

		picker = new WebcamPicker();
		picker.addItemListener(this);
		
		webcam = picker.getSelectedWebcam();

		if (webcam == null) {
			System.out.println("No webcams found...");
			System.exit(1);
		}

		webcam.setViewSize(WebcamResolution.VGA.getSize());
		webcam.addWebcamListener(Webcam3.this);

		panel = new WebcamPanel(webcam, false);
		panel.setFPSDisplayed(true);

		add(picker, BorderLayout.NORTH);
		add(panel, BorderLayout.CENTER);

		pack();
		setVisible(true);

		Thread t = new Thread() {

			@Override
			public void run() {
				panel.start();
			}
		};
		t.setName("example-starter");
		t.setDaemon(true);
		t.setUncaughtExceptionHandler(this);
		t.start();
	}
	
	public class CaptureImageAction extends AbstractAction {
		private String cPath;
	    public CaptureImageAction(String cPath) {
	      super("Capture Image");
	      this.cPath = cPath;
	    }
	    public void actionPerformed(ActionEvent ae) {
	    	try {
	    		// get image
	    		BufferedImage image = webcam.getImage();
	    		
	    		System.out.println("...saving picture.");
	    		// save image to PNG file
//	    		System.out.println("File path: " + filePath);
	    		String directoryName = this.cPath;
                File directory = new File(directoryName);
                if (! directory.exists()){
				      directory.mkdir();
				  }
	    		ImageIO.write(image, "PNG", new File(directoryName + "/passport.png"));
	    		System.out.println("Image captured!");
	    	} catch (java.io.IOException e) {
	    		e.printStackTrace();
	    	}
	    }
	}
	
	public class FileQuitAction extends AbstractAction {
	    public FileQuitAction() {
	      super("Quit");
	    }
	    public void actionPerformed(ActionEvent ae) {
	      System.exit(0);
	    }
	}

	@Override
	public void webcamOpen(WebcamEvent we) {
		System.out.println("webcam open");
	}

	@Override
	public void webcamClosed(WebcamEvent we) {
		System.out.println("webcam closed");
	}

	@Override
	public void webcamDisposed(WebcamEvent we) {
		System.out.println("webcam disposed");
	}

	@Override
	public void webcamImageObtained(WebcamEvent we) {
		// do nothing
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		webcam.close();
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		System.out.println("webcam viewer resumed");
		panel.resume();
	}

	@Override
	public void windowIconified(WindowEvent e) {
		System.out.println("webcam viewer paused");
		panel.pause();
	}

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		System.err.println(String.format("Exception in thread %s", t.getName()));
		e.printStackTrace();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getItem() != webcam) {
			if (webcam != null) {

				panel.stop();

				remove(panel);

				webcam.removeWebcamListener(this);
				webcam.close();

				webcam = (Webcam) e.getItem();
				webcam.setViewSize(WebcamResolution.VGA.getSize());
				webcam.addWebcamListener(this);

				System.out.println("selected " + webcam.getName());

				panel = new WebcamPanel(webcam, false);
				panel.setFPSDisplayed(true);

				add(panel, BorderLayout.CENTER);
				pack();

				Thread t = new Thread() {

					@Override
					public void run() {
						panel.start();
					}
				};
				t.setName("example-stoper");
				t.setDaemon(true);
				t.setUncaughtExceptionHandler(this);
				t.start();
			}
		}
	}

	@Override
	public void webcamFound(WebcamDiscoveryEvent event) {
		if (picker != null) {
			picker.addItem(event.getWebcam());
		}
	}

	@Override
	public void webcamGone(WebcamDiscoveryEvent event) {
		if (picker != null) {
			picker.removeItem(event.getWebcam());
		}
	}
}

