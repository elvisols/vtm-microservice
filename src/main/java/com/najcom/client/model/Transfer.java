package com.najcom.client.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the transfers database table.
 * 
 */
public class Transfer implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotNull(message="Transfer amount parameter missing!")
	@Digits(integer=11, fraction=2, message = "The value of amount can not be more than 7 digits")
	private BigDecimal amount;

	@NotEmpty(message="Please specify a credit account name!")
	private String creditAccountName;

	@NotNull(message="Account number parameter missing!")
//	@Digits(integer=11, fraction=0)
	private String debitAccount;

//	@NotNull(message="Account phone parameter missing!")
//	@Pattern(regexp="[0-9]{11}", message="Account phone number must comprise 11 digits only!")
//	private String debitaccountphone;

	@NotEmpty(message="Please specify an account name!")
	@Length(max = 50, min = 5, message = "The message narration should be within 5 to 50 characters")
	private String narration;

	@NotNull(message="Credit account number parameter missing!")
//	@Digits(integer=11, fraction=0)
	private String creditAccount;

	@Size(min = 3, message = "Bank code must be minimum of 3 characters!")
	private String bankCode;

//	@Override
//	public String toString() {
//		return "Amount: " + this.amount + " DebitAccountName: " + this.debitaccountname +
//				" DebitAccountNumber: " + this.debitaccountno + " DebitAccountPhone: " + this.debitaccountphone +
//				" Description: " + this.description + " ReceiverAccount: " + this.receiveraccount + " ReceiverBank: " + this.receiverbank;
//	}
	
	public Transfer() {
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getCreditAccountName() {
		return creditAccountName;
	}

	public void setCreditAccountName(String creditAccountName) {
		this.creditAccountName = creditAccountName;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

}