package com.najcom.client.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * The persistent class for the users database table.
 * 
 */
public class AddressDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@NotNull(message="Address1 parameter missing!")
	@Size(min = 10, message = "Address1 must be minimum of 10 characters!")
	private String address1;
	
	@Size(min = 10, message = "Address2 must be minimum of 10 characters!")
	private String address2;
	
	@NotEmpty(message="Please specify address type!")
	private String addressType;
	
	@NotEmpty(message="Please specify city!")
	private String city;
	
	@NotEmpty(message="Please specify state!")
	private String state;
	

	public AddressDetail() {
	}


	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addresstype) {
		this.addressType = addresstype;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}