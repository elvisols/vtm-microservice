package com.najcom.client.model;

import javax.validation.Valid;


/**
 * The persistent class for the users database table.
 * 
 */
public class Account {
	
	private String reference;
	
	@Valid
	private PersonalDetail personalDetail;

	@Valid
	private AddressDetail addressDetail;
	
	@Valid
	private DocumentDetail documentDetail;
	

	public Account() {
	}

	public AddressDetail getAddressDetail() {
		return addressDetail;
	}


	public void setAddressDetail(AddressDetail addressDetail) {
		this.addressDetail = addressDetail;
	}


	public DocumentDetail getDocumentDetail() {
		return documentDetail;
	}


	public void setDocumentDetail(DocumentDetail documentDetail) {
		this.documentDetail = documentDetail;
	}


	public String getReference() {
		return reference;
	}


	public void setReference(String reference) {
		this.reference = reference;
	}

	public PersonalDetail getPersonalDetail() {
		return personalDetail;
	}

	public void setPersonalDetail(PersonalDetail personalDetail) {
		this.personalDetail = personalDetail;
	}

}