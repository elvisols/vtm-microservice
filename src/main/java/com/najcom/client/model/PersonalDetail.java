package com.najcom.client.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * The persistent class for the users database table.
 * 
 */
public class PersonalDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
			   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	@NotEmpty(message="Please specify title!")
	private String title;

	@NotNull(message="Email parameter missing!")
	@Pattern(regexp=EMAIL_PATTERN, message="Please enter your correct email address!")
	private String email;

	@NotEmpty(message="Please specify firstname!")
	private String firstName;

	@NotEmpty(message="Please specify lastname!")
	private String lastName;
	
	@NotEmpty(message="Please specify middlename!")
	private String middleName;
	
	@NotEmpty(message="Please specify marital status!")
	private String maritalStatus;
	
	@NotEmpty(message="Please specify sex!")
	private String sex;
	
	@NotEmpty(message="Please specify state of origin!")
	private String stateOfOrigin;
	
	@NotEmpty(message="Please specify bvn!")
	private String bvn;
	
	@NotEmpty(message="Please specify tax id number!")
	private String tin;
	
	@NotEmpty(message="Please specify spousename!")
	private String spouseName;
	
	@NotEmpty(message="Please specify resident country!")
	private String residentCountry;
	
	@NotEmpty(message="Please specify birth country!")
	private String birthCountry;
	
	@NotNull(message="Date of birth must not be null !")
	@DateTimeFormat(pattern="yyyy-dd-MM")
	@Past(message = "Add an appropriate date")
	private Date dateOfBirth;

	@NotNull(message="Phone parameter missing!")
	@Pattern(regexp="[0-9]{11,14}", message="Phone number must comprise 11 digits only!")
	private String mobileNumber;

	@NotEmpty(message="Please specify religion!")
	@Length(min=3, max=20, message="Religion characters must be up to 3 and max 20 characters!")
	private String religion;

	public PersonalDetail() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBvn() {
		return bvn;
	}

	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getStateOfOrigin() {
		return stateOfOrigin;
	}

	public void setStateOfOrigin(String stateOfOrigin) {
		this.stateOfOrigin = stateOfOrigin;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getResidentCountry() {
		return residentCountry;
	}

	public void setResidentCountry(String residentCountry) {
		this.residentCountry = residentCountry;
	}

	public String getBirthCountry() {
		return birthCountry;
	}

	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

}