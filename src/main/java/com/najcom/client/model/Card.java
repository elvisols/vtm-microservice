package com.najcom.client.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;


/**
 * The persistent class for the cards database table.
 * 
 */
public class Card implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message="Please specify a debit account name!")
	private String accountname;

	@NotNull(message="Account number parameter missing!")
	@Digits(integer=10, fraction=0)
	private String accountno;

	@NotNull(message="Account phone parameter missing!")
	@Pattern(regexp="[0-9]{11}", message="Account phone number must comprise 11 digits only!")
	private String accountphone;

	@NotEmpty(message="Please specify a category! Either Issuance or Hotlisting.")
	private String category;

	@NotEmpty(message="Please specify the name of card!")
	private String nameoncard;

	@NotEmpty(message="Please specify an account type!")
	private String type;

	@Override
	public String toString() {
		return "AccountName: " + this.accountname + " AccountNo: " + this.accountno + 
				" AccountPhone: " + this.accountphone + " AccountCategory: " + this.category + 
				" NameOnCard: " + this.nameoncard + " AccountType: " + this.type;
	}
	
	public Card() {
	}

	public String getAccountname() {
		return this.accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	public String getAccountno() {
		return this.accountno;
	}

	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}

	public String getAccountphone() {
		return this.accountphone;
	}

	public void setAccountphone(String accountphone) {
		this.accountphone = accountphone;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNameoncard() {
		return this.nameoncard;
	}

	public void setNameoncard(String nameoncard) {
		this.nameoncard = nameoncard;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}