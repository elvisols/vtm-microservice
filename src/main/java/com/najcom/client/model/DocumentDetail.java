package com.najcom.client.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * The persistent class for the users database table.
 * 
 */
public class DocumentDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
			   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@NotEmpty(message="Please specify utility bill!")
	private String utilityHash;
	
	@NotEmpty(message="Please specify identification!")
	private String idHash;
	
	@NotEmpty(message="Please specify passport!")
	private String passportHash;

	public DocumentDetail() {
	}

	public String getUtilityHash() {
		return utilityHash;
	}

	public void setUtilityHash(String utilityHash) {
		this.utilityHash = utilityHash;
	}

	public String getIdHash() {
		return idHash;
	}

	public void setIdHash(String idHash) {
		this.idHash = idHash;
	}

	public String getPassportHash() {
		return passportHash;
	}

	public void setPassportHash(String passportHash) {
		this.passportHash = passportHash;
	}

}