package com.najcom.client.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class ModuleServiceImpl implements ModuleService {

	@Override
	public Map<String, Object> findModules() {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:8080/vtmserver/modules/vtm/2" );
//			String input = "{\"messages\":[{\"from\":\"InfoSMS\",\"destinations\":[{\"to\":\""+phone+"\"}],\"text\":\""+msg+"\",\"flash\":false}]}";
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYWNoaW5lMSIsImF1ZGllbmNlIjoid2ViIiwiY3JlYXRlZCI6MTQ5MzI0NjgyNDMxMSwiZXhwIjoxNDkzMzMzMjI0fQ.OQjCquiXHBnDWU6xx97_MgHLuOnpM6lIcVMQAq-44yCK9ei64iStB-T1_rhNYitMWji8Xj6AlXNnbkTWqVqU6w")
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
//			System.out.println("Server response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
//			System.out.println("\nOutput token from Server:\n" + jsonData.getOrDefault("token", "No token!"));
//			vtm = new Vtm(jsonData.get("username").toString(), jsonData.get("password").toString());
//			vtm.setId(Long.parseLong(jsonData.get("id").toString()));
//            vtm.setName(jsonData.get("firstname").toString());
//            vtm.setBank(jsonData.get("address").toString());
//            vtm.setUsername(jsonData.get("username").toString());
//            vtm.setPassword(jsonData.get("password").toString());
//            vtm.setAuthorities(jsonData.get("authorities").toString());
//            AuthorityUtils.commaSeparatedStringToAuthorityList(vtm.getAuthorities());
//            System.out.println(vtm);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}
    
}
