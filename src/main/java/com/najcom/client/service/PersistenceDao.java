package com.najcom.client.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.najcom.client.model.Persistence;

public interface PersistenceDao extends CrudRepository<Persistence, Long> {

	Persistence save(Persistence o);

    List<Persistence> findByRef(String ref);

    Persistence findByRefAndAction(String ref, String action);

    Persistence findByActionAndProcessed(String action, Integer processed);

    List<Persistence> findByDate(Date date);

	// custom query example and return a stream
//    @Query("select * from Monitor m where m.action = :action and m.ref = :ref")
//    Stream<Persistence> findByActionReturnStream(@Param("ref") String ref, @Param("action") String action);

}