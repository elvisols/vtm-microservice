package com.najcom.client.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.najcom.client.model.Card;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class CardServiceImpl implements CardService {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Override
	public Map<String, Object> sendIssuance(Card cardBody, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/card/issuance");
			String input = mapper.writeValueAsString(cardBody);
			System.out.println("Input: " + input);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
//			if (response.getStatus() != 200) {
//				System.out.println(response.getEntity(String.class));
//				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
//			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter card response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}

	@Override
	public Map<String, Object> sendHotlisting(Card cardBody, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/card/hotlisting");
			String input = mapper.writeValueAsString(cardBody);
			System.out.println("Input: " + input);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Intra card response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}
    
}
