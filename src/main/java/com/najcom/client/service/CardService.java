package com.najcom.client.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.najcom.client.model.Card;

public interface CardService {

	public Map<String, Object> sendIssuance(Card card, HttpServletRequest request);
	
	public Map<String, Object> sendHotlisting(Card transfer, HttpServletRequest request);
	
}
