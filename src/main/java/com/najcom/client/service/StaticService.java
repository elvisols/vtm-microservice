package com.najcom.client.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.najcom.client.model.Transfer;

public interface StaticService {

	public Map<String, Object> getStates(HttpServletRequest request);
	
	public Map<String, Object> getTitles(HttpServletRequest request);
	
}
