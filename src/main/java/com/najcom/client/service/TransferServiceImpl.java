package com.najcom.client.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.najcom.client.model.Transfer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class TransferServiceImpl implements TransferService {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Override
	public Map<String, Object> transferInter(Transfer transferBody, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/transfer/interbank");
			String input = mapper.writeValueAsString(transferBody);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
//			if (response.getStatus() != 200) {
//				System.out.println(response.getEntity(String.class));
//				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
//			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}

	@Override
	public Map<String, Object> transferIntra(Transfer transferBody, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/transfer/intrabank");
			String input = mapper.writeValueAsString(transferBody);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}
	
	@Override
	public Map<String, Object> transferOwnAccount(Transfer transferBody, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/transfer/own");
			String input = mapper.writeValueAsString(transferBody);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}

	@Override
	public Map<String, Object> getCreditAccount(String acc, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/transfer/credit-account-details/" + acc);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter transfer response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}

	@Override
	public Map<String, Object> getCreditAccount(String acc, String code, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/transfer/credit-account-details/" + acc + "/bankcode/" + code);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter transfer response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}

	@Override
	public Map<String, Object> getBanks(HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/transfer/banks");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter transfer response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}
	
}
