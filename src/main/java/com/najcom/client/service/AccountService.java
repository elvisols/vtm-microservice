package com.najcom.client.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;

import com.najcom.client.model.PersonalDetail;
import com.najcom.client.util.AccountOpener;

public interface AccountService {

	public Map<String, Object> openAccount(AccountOpener acc) throws ClientProtocolException, IOException;
	public Map<String, Object> interAccountDetails(String acc, HttpServletRequest request);
	public Map<String, Object> intraAccountDetails(String acc, HttpServletRequest request);
	public Map<String, Object> getAccountStatement(HttpServletRequest request);
	public Map<String, Object> getMeansOfId(HttpServletRequest request);
	public Map<String, Object> transferFiles(String owner, HttpServletRequest request) throws ClientProtocolException, IOException;
	
}
