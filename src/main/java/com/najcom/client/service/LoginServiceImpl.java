package com.najcom.client.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class LoginServiceImpl implements LoginService {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Override
	public Map<String, Object> doAuth(String username, String password) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/auth");
			String input = "{\"username\": \""+username+"\",\"password\": \""+password+"\"}";
			ClientResponse response = webResource.type("application/json")
					.post(ClientResponse.class, input);
			
//			if (response.getStatus() != 200) {
//				System.out.println(response.getEntity(String.class));
//				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
//			}
			
			String output = response.getEntity(String.class); 
			System.out.println("Login Response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}
    
}
