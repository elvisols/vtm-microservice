package com.najcom.client.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.najcom.client.model.Transfer;

public interface TransferService {

	public Map<String, Object> transferInter(Transfer transfer, HttpServletRequest request);
	
	public Map<String, Object> transferIntra(Transfer transfer, HttpServletRequest request);
	
	public Map<String, Object> transferOwnAccount(Transfer transfer, HttpServletRequest request);
	
	public Map<String, Object> getCreditAccount(String acc, HttpServletRequest request);
	
	public Map<String, Object> getCreditAccount(String acc, String code, HttpServletRequest request);
	
	public Map<String, Object> getBanks(HttpServletRequest request);
	
}
