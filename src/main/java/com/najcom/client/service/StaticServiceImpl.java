package com.najcom.client.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.najcom.client.model.Transfer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class StaticServiceImpl implements StaticService {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Override
	public Map<String, Object> getStates(HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/static/states");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter transfer response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;
	}

	@Override
	public Map<String, Object> getTitles(HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/static/titles");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			String output = response.getEntity(String.class); 
			System.out.println("Server Inter transfer response: " + output);
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}

}
