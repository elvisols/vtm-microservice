package com.najcom.client.service;

import java.util.Map;

public interface ModuleService {

	public Map<String, Object> findModules();
	
}
