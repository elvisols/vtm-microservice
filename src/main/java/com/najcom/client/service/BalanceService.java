package com.najcom.client.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.najcom.client.model.Transfer;

public interface BalanceService {

	public Map<String, Object> getBalance(String id, HttpServletRequest request);
	
}
