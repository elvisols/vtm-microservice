package com.najcom.client.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.najcom.client.model.PersonalDetail;
import com.najcom.client.util.AccountOpener;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class AccountServiceImpl implements AccountService {

	@Value("${vtm.server.path}")
	private String serverUrl;
	
	@Value("${vtm.file.capture.path}")
	private String capturePath;

	@Override
	public Map<String, Object> openAccount(AccountOpener accountBody) throws ClientProtocolException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String input = mapper.writeValueAsString(accountBody);
		Map<String, Object> jsonData = new HashMap<>();
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost uploadFile = new HttpPost(serverUrl + "/account");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addTextBody("reference", accountBody.getReference(), ContentType.TEXT_PLAIN);
		builder.addTextBody("user", input, ContentType.APPLICATION_JSON);
		
		String directoryName = capturePath + "/" + accountBody.getReference();
		File directory = new File(directoryName);
        if (! directory.exists()){
		      directory.mkdir();
		}
		// This attaches the file to the POST:
		File passportFile = new File(directoryName + "/passport.png");
		File utilityFile = new File(directoryName + "/utility.png");
		File identificationFile = new File(directoryName + "/identification.png");
		builder.addBinaryBody("passport", new FileInputStream(passportFile), ContentType.APPLICATION_OCTET_STREAM, passportFile.getName());
		builder.addBinaryBody("utility", new FileInputStream(utilityFile), ContentType.APPLICATION_OCTET_STREAM, utilityFile.getName());
		builder.addBinaryBody("identification", new FileInputStream(identificationFile), ContentType.APPLICATION_OCTET_STREAM, identificationFile.getName());

		HttpEntity multipart = builder.build();
		uploadFile.setEntity(multipart);
		uploadFile.setHeader("Authorization", accountBody.getToken());
		uploadFile.setHeader("machine", "2");
		CloseableHttpResponse response = httpClient.execute(uploadFile);
		System.out.println("Response: " + response.toString());
		HttpEntity responseEntity = response.getEntity();
		JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
		jsonData = parser.parseJson(EntityUtils.toString(responseEntity, "UTF-8"));
		
		return jsonData;
		/*Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			ObjectMapper mapper = new ObjectMapper();
			WebResource webResource = client.resource(serverUrl + "/account");
			String input = mapper.writeValueAsString(accountBody);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.post(ClientResponse.class, input);
			
			if (response.getStatus() != 200 && response.getStatus() != 201) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			// Transfer files
			this.transferFiles(accountBody.getReference(), request);
			
			String output = response.getEntity(String.class); 
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return jsonData;*/
	}
	
	@Override
	public Map<String, Object> intraAccountDetails(String account, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/account/intra/" + account);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}
	
	@Override
	public Map<String, Object> interAccountDetails(String account, HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/account/intra/" + account);
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}
	
	@Override
	public Map<String, Object> getAccountStatement(HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/account/statements");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}
	
	@Override
	public Map<String, Object> getMeansOfId(HttpServletRequest request) {
		Map<String, Object> jsonData = new HashMap<>();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(serverUrl + "/static/meansOfId");
			ClientResponse response = webResource.type("application/json")
					.header("Authorization", request.getHeader("token"))
					.get(ClientResponse.class);
			
			if (response.getStatus() != 200) {
				System.out.println(response.getEntity(String.class));
				throw new RuntimeException("Failed : HTTP error code : "	+ response.getStatus());
			}
			
			String output = response.getEntity(String.class); 
			JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
			jsonData = parser.parseJson(output);
			return jsonData;
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return jsonData;
	}
	
	@Override
	@SuppressWarnings({ "deprecation", "resource" })
	public Map<String, Object> transferFiles(String owner, HttpServletRequest request) throws ClientProtocolException, IOException {
		Map<String, Object> jsonData = new HashMap<>();
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost uploadFile = new HttpPost(serverUrl + "/files/upload");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addTextBody("token", request.getHeader("token"), ContentType.TEXT_PLAIN);
		String directoryName = capturePath + "/" + owner;
		File directory = new File(directoryName);
        if (! directory.exists()){
		      directory.mkdir();
		}
		// This attaches the file to the POST:
		File passportFile = new File(directoryName + "/passport.png");
		File utilityFile = new File(directoryName + "/utility.png");
		File identificationFile = new File(directoryName + "/identification.png");
		builder.addBinaryBody("passport", new FileInputStream(passportFile), ContentType.APPLICATION_OCTET_STREAM, passportFile.getName());
		builder.addBinaryBody("utility", new FileInputStream(utilityFile), ContentType.APPLICATION_OCTET_STREAM, utilityFile.getName());
		builder.addBinaryBody("identification", new FileInputStream(identificationFile), ContentType.APPLICATION_OCTET_STREAM, identificationFile.getName());

		HttpEntity multipart = builder.build();
		uploadFile.setEntity(multipart);
		uploadFile.setHeader("Authorization", request.getHeader("token"));
		uploadFile.setHeader("machine", "2");
		CloseableHttpResponse response = httpClient.execute(uploadFile);
		System.out.println("Response: " + response.toString());
		HttpEntity responseEntity = response.getEntity();
		JSONParser parser = JsonParserFactory.getInstance().newJsonParser();
		jsonData = parser.parseJson(EntityUtils.toString(responseEntity, "UTF-8"));
		
		return jsonData;
		
//		Client client = Client.create();
//		WebResource webResource = client.resource("http://localhost:8080/vtmserver/files/upload");
//		File file = new File("D:\\najcom\\JavaAzureAppGuide.pdf");
//		try {
//			FormDataMultiPart multiPart = new FormDataMultiPart();
//			multiPart.field("file", "hey");
////			 multiPart.field("file", user);
//			// multiPart.field("pass", pass);
//			// multiPart.field("task_code", task_code);
//
////			multiPart.bodyPart(new FileDataBodyPart("files[]", file));
////			multiPart.bodyPart(new FileDataBodyPart("file", file));
//			ClientResponse response = webResource
//					.type(MediaType.MULTIPART_FORM_DATA)
//					.accept(MediaType.TEXT_PLAIN)
//					.post(ClientResponse.class, multiPart);
//
//			if (response.getStatus() == 200) {
//				System.out.println("File upload successfully.");
//			} else {
//				System.out.println("File cannot be uploaded. STATUS = "
//						+ response.getStatus());
//			}
//		} catch (ClientHandlerException e) {
//
//		}
	}
    
}
