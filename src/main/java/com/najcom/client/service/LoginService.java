package com.najcom.client.service;

import java.util.Map;

public interface LoginService {

	public Map<String, Object> doAuth(String username, String password);
	
}
